pncd.README

This is version 0.40.

What is pncd?
-------------

pncd.c is a single Panasonic/Matsushita CR-56x CDROM driver for Linux.
It is tested with linux 2.4.x as a module with a
no-sound interface card, a GUS MAX soundcard and SoundBlaster 16.  
This driver is intended to be a replacement for the sbpcd 
for the Panasonic CR-56x CDROM drives.  

The program is Copyright (C) Zoltan Vorosbaranyi and is a free software
(see the GNU Public Licence). 

Brian Candler (B.Candler@pobox.com) made bug fixes and modifications 
for the version 0.21.


Change log:

version 0.40
------------
  o port to 2.4.x
  o renamed to pncd

version 0.38
------------
  o bug fixes

version 0.37
------------
  o audio frame reading code rewrite
  o minor bugfixes

version 0.36
------------
  o supports reading audio frames
  o supports reading MCN (2.2.x kernels)
  o drive speed settable (2.2.x kernels)
  o multiple opens handled safely (2.2.x kernels)

version 0.35
------------
  o support for kernel version 2.2.x
  o cooperating with the Unified Driver

version 0.30
------------
  o the device can now be opened without disk in drive
  o minor modifications in ioctl
  o authors e-mail changed to vbzoli@vbzo.li
  o web page http://vbzo.li/pcd/

version 0.29
------------
  o added id=N command line parameter
  o added drive id hunting
  o modified to work with 2.1.xx kernels

version 0.21
------------
  o added io=0xNNN command line parameter for insmod to set base addr
  o fixed cache bug
  o 512-byte blocks are now allowed (for Macintosh HFS)

version 0.2
-----------
  o autodetection of base ports
  o SoundBlaster interface support


What are the differences between the sbpcd and pcd?
---------------------------------------------------

Advantages over the sbpcd driver:
  o much smaller
  o uses less CPU
  o disk change detection
  o works with Workman 1.3a (I learned form Dirk Foersterling
    <milliByte@DeathsDoor.com>, that WorkMan now has a workaround for sbpcd,
    so new WorkMan releases should work)
  o supports reset-less initialization
  o detects the size of the data track
  o device can be opened with no disk in the drive

Disadvantages:
  o supports only single drive (I only have one drive)
  o doesn't support other CD drives


How to install?
---------------

1. Compile the kernel with sbpcd support.
2. OPTIONAL: Read comments at the beginning of the source file (pcd.c).
3. OPTIONAL: Set the base address of your interface (PCD_BASEPORT),
   and set drive id (PCD_DRIVE_ID).  
   NOTE: You can set the base address by the io=0xNNN, drive id
         by id=M command line parameter (see below)
4. Compile and install the module.
   I use the following script to compile and install
-----------------------------------
gcc -D__KERNEL__ -I/usr/src/linux/include -Wall -Wstrict-prototypes -O2 -fomit-frame-pointer -pipe -m486 -DMODULE  -c pcd.c
cp pcd.o /lib/modules/`uname -r`/cdrom/ 
-----------------------------------
   You may want to replace `uname -r` with the word preferred on Red Hat
   systems.
   Add any other optimization flags, but -DLINUX_2, -DOLD is obsolete now. 
   The driver detects the kernel version automatically.
5. Insert module (with optional base address): 
insmod pcd
   - or -
insmod pcd io=0xNNN id=M
   The M (jumper settable) drive id is usually 0 and you can omit it in that
   case as this is the default.   0xNNN is the base address of the interface.
   - or -
   alternatively you can use the kerneld utility to insert the driver
   (and other) modules on-demand.  (You may want to first read the kerneld 
   mini-howto, the README in the modules-2.0.0 package (or later)). Insert the 
   following line in the /etc/conf.modules file after you have created it
   (nowadays kerneld is configured properly):
-----------------------------------
alias block-major-25 pcd
options pcd io=0xNNN id=M
-----------------------------------
   and run 
depmod -a

--

